<?php
namespace Craft;

/**
 * Class Lead record.
 *
 * Provides a definition of the database tables for this plugin.
 * This class should only be called by the service layer.
 *
 * @package Craft
 *
 */
class LeadsRecord extends BaseRecord
{
    /**
     * Return the name of the database table for this plugin.
     *
     * @return string
     *
     */
    public function getTableName()
    {
        return 'leads';
    }

    /**
     * Define the table attributes for this plugin.
     *
     * @return array
     *
     */
    public function defineAttributes()
    {
        return array(
            'name' => array(AttributeType::Name, 'default' => null),
            'email' => array(AttributeType::Email, 'default' => null),
            'status' => array(AttributeType::Name, 'default' => null),
            'comment' => array(AttributeType::Name, 'default' => null)
        );
    }
}