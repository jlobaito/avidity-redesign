<?php
namespace Craft;

/**
 * Leads controller.
 *
 * Defines actions which can be posted to by forms within the templates.
 *
 * @package Craft
 *
 */
class LeadsController extends BaseController
{
    protected $allowAnonymous = true;

    public function actionSaveLead()
    {
        $this->requireAjaxRequest();

        $lead = new LeadsModel();

        $lead->name = craft()->request->getPost('name');
        $lead->email = craft()->request->getPost('email');
        $lead->status = craft()->request->getPost('status');
        $lead->comment = craft()->request->getPost('comment');

        if ($lead->validate()) {
            if (craft()->leads->saveLead($lead)) {
                $this->returnJson(array('success' => true));
            } else {
                $this->returnJson(array('success' => false));
            }
        } else {
            if (craft()->request->isAjaxRequest()) {
                return $this->returnErrorJson($lead->getErrors());
            } else {
                craft()->urlManager->setRouteVariables(array(
                    'lead' => $lead
                ));

                return $lead->getErrors();
            }
        }
    }


    /**
     * List all contacts within the CP.
     *
     * @return void
     *
     */
    public function actionListLeads()
    {
        $vars['leads'] = craft()->lead->listLeads();

        $this->renderTemplate('leads/index', $vars);
    }

    /**
     * Delete the given lead.
     *
     * @throws HttpException
     *
     */
    public function actionDeleteLead()
    {
        $this->requireAjaxRequest();
        $leadId = craft()->request->getPost('id');

        if (craft()->leads->deleteLead($leadId)) {
            $response = array('status' => 'success', 'comment' => null);
        } else {
            $response = array('status' => 'fail', 'comment' => 'Could not delete.');
        }

        header('Content-Type: application/json');

        echo json_encode($response);
    }
}